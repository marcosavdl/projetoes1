insert into "TipoLogradouro"("siglaLogradouro","nomeTipo") values('R','Rua');
insert into "TipoLogradouro"("siglaLogradouro","nomeTipo") values('Av','Avenida');
insert into "TipoLogradouro"("siglaLogradouro","nomeTipo") values('Pq','Parque');

insert into "Logradouro"("nomeLogradouro","siglaLogradouro_TipoLogradouro") values('Gramado','Av');
insert into "Logradouro"("nomeLogradouro","siglaLogradouro_TipoLogradouro") values('Áustria','R');
insert into "Logradouro"("nomeLogradouro","siglaLogradouro_TipoLogradouro") values('Cristal de Rocha','R');
insert into "Logradouro"("nomeLogradouro","siglaLogradouro_TipoLogradouro") values('Trinta e Sete','R');
insert into "Logradouro"("nomeLogradouro","siglaLogradouro_TipoLogradouro") values('Hermann Spernau','R');
insert into "Logradouro"("nomeLogradouro","siglaLogradouro_TipoLogradouro") values('Estação da Luz','R');
insert into "Logradouro"("nomeLogradouro","siglaLogradouro_TipoLogradouro") values('Ceará','R');
insert into "Logradouro"("nomeLogradouro","siglaLogradouro_TipoLogradouro") values('Antônio João','R');
insert into "Logradouro"("nomeLogradouro","siglaLogradouro_TipoLogradouro") values('Doutor Nilo Peçanha','Av');
insert into "Logradouro"("nomeLogradouro","siglaLogradouro_TipoLogradouro") values('Anselmo da Silva Cordova','R');
insert into "Logradouro"("nomeLogradouro","siglaLogradouro_TipoLogradouro") values('Edésio Carneiro de Campo','R');
insert into "Logradouro"("nomeLogradouro","siglaLogradouro_TipoLogradouro") values('San Marino','R');
insert into "Logradouro"("nomeLogradouro","siglaLogradouro_TipoLogradouro") values('Onze','R');

insert into "Bairro"("nomeBairro") values('Jardim Lancaster');
insert into "Bairro"("nomeBairro") values('Igapó');
insert into "Bairro"("nomeBairro") values('Conjuno Residencial José Bonifácio');
insert into "Bairro"("nomeBairro") values('Terras de Imoplan');
insert into "Bairro"("nomeBairro") values('Água Verde');
insert into "Bairro"("nomeBairro") values('Sambaqui');
insert into "Bairro"("nomeBairro") values('Vila Rica');
insert into "Bairro"("nomeBairro") values('Centro');
insert into "Bairro"("nomeBairro") values('Chácara das Pedras');
insert into "Bairro"("nomeBairro") values('Boa Saúde');
insert into "Bairro"("nomeBairro") values('Jardim Panorama');
insert into "Bairro"("nomeBairro") values('Jardim Adriana');
insert into "Bairro"("nomeBairro") values('Jardim Monte Verde');

insert into "UnidadeFederacao"("siglaUF","nomeUF") values('PR','Paraná');
insert into "UnidadeFederacao"("siglaUF","nomeUF") values('SP','São Paulo');
insert into "UnidadeFederacao"("siglaUF","nomeUF") values('SC','Santa Catarina');
insert into "UnidadeFederacao"("siglaUF","nomeUF") values('MS','Mato Grosso do Sul');
insert into "UnidadeFederacao"("siglaUF","nomeUF") values('RS','Rio Grande do Sul');
insert into "UnidadeFederacao"("siglaUF","nomeUF") values('MG','Minas Gerais');

insert into "Cidade"("nomeCidade","siglaUF_UnidadeFederacao") values('Foz" do Iguaçu','PR');
insert into "Cidade"("nomeCidade","siglaUF_UnidadeFederacao") values('Lon"drina','PR');
insert into "Cidade"("nomeCidade","siglaUF_UnidadeFederacao") values('São" Paulo','SP');
insert into "Cidade"("nomeCidade","siglaUF_UnidadeFederacao") values('Pre"sidente Prudente','SP');
insert into "Cidade"("nomeCidade","siglaUF_UnidadeFederacao") values('Blu"menau','SC');
insert into "Cidade"("nomeCidade","siglaUF_UnidadeFederacao") values('Flo"rianópolis','SC');
insert into "Cidade"("nomeCidade","siglaUF_UnidadeFederacao") values('Cam"po Grande','MS');
insert into "Cidade"("nomeCidade","siglaUF_UnidadeFederacao") values('Cox"im','MS');
insert into "Cidade"("nomeCidade","siglaUF_UnidadeFederacao") values('Por"to Alegre','RS');
insert into "Cidade"("nomeCidade","siglaUF_UnidadeFederacao") values('Nov"o Hamburgo','RS');

insert into "Endereco"("CEP","idLogradouro_Logradouro","idCidade_Cidade","idBairro_Bairro") values('85869160',1,1,1);
insert into "Endereco"("CEP","idLogradouro_Logradouro","idCidade_Cidade","idBairro_Bairro") values('86046320',2,2,2);
insert into "Endereco"("CEP","idLogradouro_Logradouro","idCidade_Cidade","idBairro_Bairro") values('08255225',3,3,3);
insert into "Endereco"("CEP","idLogradouro_Logradouro","idCidade_Cidade","idBairro_Bairro") values('19044135',4,4,4);
insert into "Endereco"("CEP","idLogradouro_Logradouro","idCidade_Cidade","idBairro_Bairro") values('89037506',5,5,5);
insert into "Endereco"("CEP","idLogradouro_Logradouro","idCidade_Cidade","idBairro_Bairro") values('88051405',6,6,6);
insert into "Endereco"("CEP","idLogradouro_Logradouro","idCidade_Cidade","idBairro_Bairro") values('79022970',7,7,7);
insert into "Endereco"("CEP","idLogradouro_Logradouro","idCidade_Cidade","idBairro_Bairro") values('79400970',8,8,8);
insert into "Endereco"("CEP","idLogradouro_Logradouro","idCidade_Cidade","idBairro_Bairro") values('91330001',9,9,9);
insert into "Endereco"("CEP","idLogradouro_Logradouro","idCidade_Cidade","idBairro_Bairro") values('93347170',10,10,10);
insert into "Endereco"("CEP","idLogradouro_Logradouro","idCidade_Cidade","idBairro_Bairro") values('85856570',11,1,11);
insert into "Endereco"("CEP","idLogradouro_Logradouro","idCidade_Cidade","idBairro_Bairro") values('86046250',12,2,12);
insert into "Endereco"("CEP","idLogradouro_Logradouro","idCidade_Cidade","idBairro_Bairro") values('04851528',13,3,13);

insert into "email"("id_email","enderecoEmail") values(1,'marcosavdl@live.com');
insert into "email"("id_email","enderecoEmail") values(2,'marcosavdl@gmail.com');

insert into "DDI"("ddi_number","Pais") values('+55','Brasil');
insert into "DDI"("ddi_number","Pais") values('+93','Afghanistan');
insert into "DDI"("ddi_number","Pais") values('+355','Albania');
insert into "DDI"("ddi_number","Pais") values('+213','Algeria');
insert into "DDI"("ddi_number","Pais") values('+244','Angola');
insert into "DDI"("ddi_number","Pais") values('+54','Argentina');
insert into "DDI"("ddi_number","Pais") values('+374','Armenia');

insert into "DDD"("pk_ddd","ddi_number_DDI") values('11','+55');
insert into "DDD"("pk_ddd","ddi_number_DDI") values('12','+55');
insert into "DDD"("pk_ddd","ddi_number_DDI") values('13','+55');
insert into "DDD"("pk_ddd","ddi_number_DDI") values('14','+55');
insert into "DDD"("pk_ddd","ddi_number_DDI") values('45','+55');

insert into "telefone"("numero","pk_ddd_DDD") values(88088965,45);
insert into "telefone"("numero","pk_ddd_DDD") values(88088456,45);
insert into "telefone"("numero","pk_ddd_DDD") values(88076899,45);
insert into "telefone"("numero","pk_ddd_DDD") values(88023332,45);

insert into "cadastroEstabelecimento"("idEstabelecimento","razaoSocial"                             ,"nomeFantasia"     ,"ativoEstabelecimento","cnpjEstabelecimento","IE"          ,"lat","long","complemento","numero","idEndereco_Endereco","id_email_email","id_telefone_telefone")
                               values(1                  ,'Matheus e Beatriz Pizzaria Delivery Ltda','Matheus e Beatriz',true                  ,'86651572000156'     ,'630599110043',123  ,321   ,'no centro'  ,370      ,1                   ,1               , 1                    );


insert into "Mesa"("numeroMesa","localizacaoMesa","idEstabelecimento_cadastroEstabelecimento") values('1','Janela',1);
insert into "Mesa"("numeroMesa","localizacaoMesa","idEstabelecimento_cadastroEstabelecimento") values('2','Centro',1);
insert into "Mesa"("numeroMesa","localizacaoMesa","idEstabelecimento_cadastroEstabelecimento") values('3','Centro',1);
insert into "Mesa"("numeroMesa","localizacaoMesa","idEstabelecimento_cadastroEstabelecimento") values('4','Janela',1);




insert into "Categoria"("categoria") values('Massas');
insert into "Categoria"("categoria") values('Prato Executivo');
insert into "Categoria"("categoria") values('Bebida');
insert into "Categoria"("categoria") values('Salada');
insert into "Categoria"("categoria") values('Sobremesa');
insert into "Categoria"("categoria") values('Porcoes');
insert into "Categoria"("categoria") values('Executivo');

insert into "UnidadeMedida"("siglaUnidade","descricaoUnidade") values('kg','KiloGrama');
insert into "UnidadeMedida"("siglaUnidade","descricaoUnidade") values('mg','MiliGrama');
insert into "UnidadeMedida"("siglaUnidade","descricaoUnidade") values('L','Litro');
insert into "UnidadeMedida"("siglaUnidade","descricaoUnidade") values('ml','Mililitro');
insert into "UnidadeMedida"("siglaUnidade","descricaoUnidade") values('un','Unidade');

insert into "cadastroPrato"("nomePrato"        ,"valorPrato","fotoPrato","descricaoPrato"    ,"avaliacaoPrato","ativoCadastro","quantidadePrato","idEstabelecimento_cadastroEstabelecimento","categoria_Categoria","siglaUnidade_UnidadeMedida")
                     values('Bife à Parmegiana', 2999      ,'paht'     ,'descricao do prato',10              , true          ,500              ,1                                          , 'Executivo'   ,'mg'                        );
insert into "cadastroPrato"("nomePrato"        ,"valorPrato","fotoPrato","descricaoPrato"    ,"avaliacaoPrato","ativoCadastro","quantidadePrato","idEstabelecimento_cadastroEstabelecimento","categoria_Categoria","siglaUnidade_UnidadeMedida")
                     values('Filé de Linguado' , 3999      ,'paht'     ,'descricao do prato',10              , false         ,500              ,1                                          , 'Executivo'   ,'mg'                        );
insert into "cadastroPrato"("nomePrato"           ,"valorPrato","fotoPrato","descricaoPrato"    ,"avaliacaoPrato","ativoCadastro","quantidadePrato","idEstabelecimento_cadastroEstabelecimento","categoria_Categoria","siglaUnidade_UnidadeMedida")
                     values('Refrigerante (Lata)' , 580      ,'paht'     ,'descricao do prato',10              , false         ,250              ,1                                          , 'Bebida'   ,'ml'                        );
insert into "cadastroPrato"("nomePrato"         ,"valorPrato","fotoPrato","descricaoPrato"    ,"avaliacaoPrato","ativoCadastro","quantidadePrato","idEstabelecimento_cadastroEstabelecimento","categoria_Categoria","siglaUnidade_UnidadeMedida")
                     values('Suco Natural (1L)' , 1699     ,'paht'     ,'descricao do prato',10              , false         ,250              ,1                                          , 'Bebida'   ,'ml'                        );
insert into "cadastroPrato"("nomePrato"        ,"valorPrato","fotoPrato","descricaoPrato"    ,"avaliacaoPrato","ativoCadastro","quantidadePrato","idEstabelecimento_cadastroEstabelecimento","categoria_Categoria","siglaUnidade_UnidadeMedida")
                     values('Salada Caesar'    , 1699     ,'paht'     ,'descricao do prato',10              , false         ,250              ,1                                          , 'Salada'   ,'mg'                        );
insert into "cadastroPrato"("nomePrato"        ,"valorPrato","fotoPrato","descricaoPrato"    ,"avaliacaoPrato","ativoCadastro","quantidadePrato","idEstabelecimento_cadastroEstabelecimento","categoria_Categoria","siglaUnidade_UnidadeMedida")
                     values('Salada Caprese'   , 1699     ,'paht'     ,'descricao do prato',10              , true         ,250              ,1                                          , 'Salada'   ,'mg'                        );

insert into "cadastroPrato"("nomePrato"        ,"valorPrato","fotoPrato","descricaoPrato"    ,"avaliacaoPrato","ativoCadastro","quantidadePrato","idEstabelecimento_cadastroEstabelecimento","categoria_Categoria","siglaUnidade_UnidadeMedida")
                     values('Pudim'            , 1699     ,'paht'     ,'descricao do prato',10               , true          ,250              ,1                                          , 'Sobremesa'         ,'mg'                        );

insert into "cadastroPrato"("nomePrato"                    ,"valorPrato","fotoPrato","descricaoPrato"    ,"avaliacaoPrato","ativoCadastro","quantidadePrato","idEstabelecimento_cadastroEstabelecimento","categoria_Categoria","siglaUnidade_UnidadeMedida")
                     values('Mousse / Gelatina Colorida'   , 1699      ,'paht'     ,'descricao do prato',10              , true         ,250              ,1                                           , 'Sobremesa'   ,'mg'                        );
insert into "cadastroPrato"("nomePrato"                    ,"valorPrato","fotoPrato","descricaoPrato"    ,"avaliacaoPrato","ativoCadastro","quantidadePrato","idEstabelecimento_cadastroEstabelecimento","categoria_Categoria","siglaUnidade_UnidadeMedida")
                     values('Salgados (6 Unidades)'   , 1699      ,'paht'     ,'descricao do prato',10              , true         ,250              ,1                                           , 'Porcoes'   ,'mg'                        );
insert into "cadastroPrato"("nomePrato"                    ,"valorPrato","fotoPrato","descricaoPrato"    ,"avaliacaoPrato","ativoCadastro","quantidadePrato","idEstabelecimento_cadastroEstabelecimento","categoria_Categoria","siglaUnidade_UnidadeMedida")
                     values('Batata Frita'   , 1699      ,'paht'     ,'descricao do prato',10              , true         ,250              ,1                                           , 'Porcoes'   ,'mg'                        );

insert into "ItensPedido"("quantidade","valorUnitario","idPrato_cadastroPrato") values(3,10.21,1);
insert into "ItensPedido"("quantidade","valorUnitario","idPrato_cadastroPrato") values(5,89.71,2);
insert into "ItensPedido"("quantidade","valorUnitario","idPrato_cadastroPrato") values(5,16.21,3);
insert into "ItensPedido"("quantidade","valorUnitario","idPrato_cadastroPrato") values(7,10.91,4);
insert into "ItensPedido"("quantidade","valorUnitario","idPrato_cadastroPrato") values(3,16.61,5);
insert into "ItensPedido"("quantidade","valorUnitario","idPrato_cadastroPrato") values(2,14.21,6);

insert into "Pedido"("valorTotal","status","observacao","codigo_ItensPedido","pkMesa_Mesa") values(23090,'andamento','sem cebola',1,1);
insert into "Pedido"("valorTotal","status","observacao","codigo_ItensPedido","pkMesa_Mesa") values(62090,'andamento','sem cebola',2,1);
insert into "Pedido"("valorTotal","status","observacao","codigo_ItensPedido","pkMesa_Mesa") values(4090,'andamento','sem cebola',3,1);
insert into "Pedido"("valorTotal","status","observacao","codigo_ItensPedido","pkMesa_Mesa") values(80090,'andamento','sem cebola',5,3);
insert into "Pedido"("valorTotal","status","observacao","codigo_ItensPedido","pkMesa_Mesa") values(30090,'andamento','sem cebola',3,4);
insert into "Pedido"("valorTotal","status","observacao","codigo_ItensPedido","pkMesa_Mesa") values(70090,'andamento','sem cebola',1,2);
