-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler version: 0.9.4-beta
-- PostgreSQL version: 13.0
-- Project Site: pgmodeler.io
-- Model Author: ---

-- Database creation must be performed outside a multi lined SQL file. 
-- These commands were put in this file only as a convenience.
-- 
-- object: "testeHasura" | type: DATABASE --
-- DROP DATABASE IF EXISTS "testeHasura";
CREATE DATABASE "testeHasura";
-- ddl-end --


-- object: public."UnidadeFederacao" | type: TABLE --
-- DROP TABLE IF EXISTS public."UnidadeFederacao" CASCADE;
CREATE TABLE public."UnidadeFederacao" (
	"siglaUF" character varying(2) NOT NULL,
	"nomeUF" character varying(45),
	CONSTRAINT "UnidadeFederacao_pk" PRIMARY KEY ("siglaUF")

);
-- ddl-end --
ALTER TABLE public."UnidadeFederacao" OWNER TO postgres;
-- ddl-end --

-- object: public."Bairro_idBairro_seq" | type: SEQUENCE --
-- DROP SEQUENCE IF EXISTS public."Bairro_idBairro_seq" CASCADE;
CREATE SEQUENCE public."Bairro_idBairro_seq"
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	START WITH 1
	CACHE 1
	NO CYCLE
	OWNED BY NONE;

-- ddl-end --
ALTER SEQUENCE public."Bairro_idBairro_seq" OWNER TO postgres;
-- ddl-end --

-- object: public."Bairro" | type: TABLE --
-- DROP TABLE IF EXISTS public."Bairro" CASCADE;
CREATE TABLE public."Bairro" (
	"idBairro" integer NOT NULL DEFAULT nextval('public."Bairro_idBairro_seq"'::regclass),
	"nomeBairro" character varying(120),
	CONSTRAINT "Bairro_pk" PRIMARY KEY ("idBairro")

);
-- ddl-end --
ALTER TABLE public."Bairro" OWNER TO postgres;
-- ddl-end --

-- object: public."TipoLogradouro" | type: TABLE --
-- DROP TABLE IF EXISTS public."TipoLogradouro" CASCADE;
CREATE TABLE public."TipoLogradouro" (
	"siglaLogradouro" varchar(2) NOT NULL,
	"nomeTipo" character varying(45),
	CONSTRAINT "TipoLogradouro_pk" PRIMARY KEY ("siglaLogradouro")

);
-- ddl-end --
ALTER TABLE public."TipoLogradouro" OWNER TO postgres;
-- ddl-end --

-- object: public."Cidade" | type: TABLE --
-- DROP TABLE IF EXISTS public."Cidade" CASCADE;
CREATE TABLE public."Cidade" (
	"idCidade" serial NOT NULL,
	"nomeCidade" character varying(120),
	"siglaUF_UnidadeFederacao" character varying(2),
	CONSTRAINT "Cidade_pk" PRIMARY KEY ("idCidade")

);
-- ddl-end --
ALTER TABLE public."Cidade" OWNER TO postgres;
-- ddl-end --

-- object: public."Endereco" | type: TABLE --
-- DROP TABLE IF EXISTS public."Endereco" CASCADE;
CREATE TABLE public."Endereco" (
	"idEndereco" serial NOT NULL,
	"CEP" character varying(11),
	"idLogradouro_Logradouro" integer,
	"idCidade_Cidade" integer,
	"idBairro_Bairro" integer,
	CONSTRAINT "Endereco_pk" PRIMARY KEY ("idEndereco")

);
-- ddl-end --
ALTER TABLE public."Endereco" OWNER TO postgres;
-- ddl-end --

-- object: public."Logradouro" | type: TABLE --
-- DROP TABLE IF EXISTS public."Logradouro" CASCADE;
CREATE TABLE public."Logradouro" (
	"idLogradouro" serial NOT NULL,
	"nomeLogradouro" character varying(120),
	"siglaLogradouro_TipoLogradouro" varchar(2),
	CONSTRAINT "Logradouro_pk" PRIMARY KEY ("idLogradouro")

);
-- ddl-end --
ALTER TABLE public."Logradouro" OWNER TO postgres;
-- ddl-end --

-- object: "TipoLogradouro_fk" | type: CONSTRAINT --
-- ALTER TABLE public."Logradouro" DROP CONSTRAINT IF EXISTS "TipoLogradouro_fk" CASCADE;
ALTER TABLE public."Logradouro" ADD CONSTRAINT "TipoLogradouro_fk" FOREIGN KEY ("siglaLogradouro_TipoLogradouro")
REFERENCES public."TipoLogradouro" ("siglaLogradouro") MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: "Logradouro_fk" | type: CONSTRAINT --
-- ALTER TABLE public."Endereco" DROP CONSTRAINT IF EXISTS "Logradouro_fk" CASCADE;
ALTER TABLE public."Endereco" ADD CONSTRAINT "Logradouro_fk" FOREIGN KEY ("idLogradouro_Logradouro")
REFERENCES public."Logradouro" ("idLogradouro") MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: "Cidade_fk" | type: CONSTRAINT --
-- ALTER TABLE public."Endereco" DROP CONSTRAINT IF EXISTS "Cidade_fk" CASCADE;
ALTER TABLE public."Endereco" ADD CONSTRAINT "Cidade_fk" FOREIGN KEY ("idCidade_Cidade")
REFERENCES public."Cidade" ("idCidade") MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: "Bairro_fk" | type: CONSTRAINT --
-- ALTER TABLE public."Endereco" DROP CONSTRAINT IF EXISTS "Bairro_fk" CASCADE;
ALTER TABLE public."Endereco" ADD CONSTRAINT "Bairro_fk" FOREIGN KEY ("idBairro_Bairro")
REFERENCES public."Bairro" ("idBairro") MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: "UnidadeFederacao_fk" | type: CONSTRAINT --
-- ALTER TABLE public."Cidade" DROP CONSTRAINT IF EXISTS "UnidadeFederacao_fk" CASCADE;
ALTER TABLE public."Cidade" ADD CONSTRAINT "UnidadeFederacao_fk" FOREIGN KEY ("siglaUF_UnidadeFederacao")
REFERENCES public."UnidadeFederacao" ("siglaUF") MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --


