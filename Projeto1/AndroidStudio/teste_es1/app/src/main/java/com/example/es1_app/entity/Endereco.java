package com.example.es1_app.entity;

public class Endereco extends Entity{

    private String cep;
    private Cidade cidade;
    private Bairro bairro;
    private Logradouro lograd;
    private String complemento;
    private String numero;

    public Endereco() {

        cep = complemento = numero = "";
        cidade = new Cidade();
        bairro = new Bairro();
        lograd = new Logradouro();
    }

    public Endereco(String cep, Cidade cidade, Bairro bairro, Logradouro lograd) {
        this.cep = cep;
        this.cidade = cidade;
        this.bairro = bairro;
        this.lograd = lograd;
    }

    public Endereco(String cep, Cidade cidade, Bairro bairro, Logradouro lograd, String complemento, String numero) {
        this.cep = cep;
        this.cidade = cidade;
        this.bairro = bairro;
        this.lograd = lograd;
        this.complemento = complemento;
        this.numero = numero;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public Bairro getBairro() {
        return bairro;
    }

    public void setBairro(Bairro bairro) {
        this.bairro = bairro;
    }

    public Logradouro getLograd() {
        return lograd;
    }

    public void setLograd(Logradouro lograd) {
        this.lograd = lograd;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Override
    public String toString() {
        return "Endereco{" +
                "cep='" + cep + '\'' +
                ", cidade=" + cidade +
                ", bairro=" + bairro +
                ", lograd=" + lograd +
                ", complemento='" + complemento + '\'' +
                ", numero='" + numero + '\'' +
                '}';
    }
}
