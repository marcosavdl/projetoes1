package com.example.es1_app.entity;

public class UnidadeFederacao {

    private String nomeUF;

    public UnidadeFederacao(){
        nomeUF = "";
    }

    public UnidadeFederacao(String nomeUF){
        this.nomeUF = nomeUF;
    }

    public String getNomeUF() {
        return nomeUF;
    }

    public void setNomeUF(String nomeUF) {
        this.nomeUF = nomeUF;
    }

    @Override
    public String toString() {
        return "UnidadeFederacao{" +
                "nomeUF='" + nomeUF + '\'' +
                '}';
    }
}
