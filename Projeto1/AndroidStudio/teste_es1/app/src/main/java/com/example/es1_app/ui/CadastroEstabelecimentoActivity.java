package com.example.es1_app.ui;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.es1_app.R;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;

public class CadastroEstabelecimentoActivity extends AppCompatActivity {

    private TextInputEditText razaoSocial;
    private TextInputEditText nomeFantasia;
    private TextInputEditText cnpj;
    private TextInputEditText inscricaoEstadual;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_estabelecimento);

        setComponents();
        setButtonAction();
    }

    public void setComponents(){

        razaoSocial = findViewById(R.id.textInputEditText_razaoSocial);
        nomeFantasia = findViewById(R.id.textInputEditText_nomeFantasia);
        cnpj = findViewById(R.id.textInputEditText_cnpj);
        inscricaoEstadual = findViewById(R.id.textInputEditText_inscricaoEstadual);
        button = findViewById(R.id.button_cadastroEstabelecimento);
    }

    public void setButtonAction() {

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StringBuilder str = new StringBuilder();
                AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                builder.setTitle("Cadastro de Estabelecimento")
                        .setMessage("Buabua")
                        .setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Intent intent = new Intent(v.getContext(),CadastroEnderecoActivity.class);
                                startActivityIfNeeded(intent,0);
                            }
                        }).setNegativeButton("Voltar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();
                            }
                }).show();
            }
        });
    }

    public boolean validateFields(View v){

        ArrayList<TextInputEditText> list = fillTextArray();
        return true;
    }

    public ArrayList<TextInputEditText> fillTextArray(){

        ArrayList<TextInputEditText> list = new ArrayList<>();
        list.add(razaoSocial);
        list.add(nomeFantasia);
        list.add(cnpj);
        list.add(inscricaoEstadual);
        return list;
    }
}