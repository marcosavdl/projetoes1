package com.example.es1_app.entity;

public class Cidade extends Entity{

    private String nomeCidade;
    private UnidadeFederacao uf;

    public Cidade() {
        nomeCidade = "";
        uf = new UnidadeFederacao();
    }

    public Cidade(String nomeCidade){
        this.nomeCidade = nomeCidade;
    }

    public Cidade(String nomeCidade, UnidadeFederacao uf){

        this.nomeCidade = nomeCidade;
        this.uf = uf;
    }

    public String getNomeCidade() {
        return nomeCidade;
    }

    public void setNomeCidade(String nomeCidade) {
        this.nomeCidade = nomeCidade;
    }

    public UnidadeFederacao getUf() {
        return uf;
    }

    public void setUf(UnidadeFederacao uf) {
        this.uf = uf;
    }

    @Override
    public String toString() {
        return "Cidade{" +
                "nomeCidade='" + nomeCidade + '\'' +
                ", uf=" + uf +
                '}';
    }
}
