package com.example.es1_app.entity;

public class Entity {

    private Object ID;

    public Entity(){

    }

    public Entity(Object obj){
        this.ID = obj;
    }

    public Object getID() {
        return ID;
    }

    public void setID(Object ID) {
        this.ID = ID;
    }
}
