package com.example.es1_app.entity;

public class Bairro extends Entity{

    private String nomeBairro;

    public Bairro() {
        nomeBairro = "";
    }

    public Bairro(String nomeBairro) {
        this.nomeBairro = nomeBairro;
    }

    public String getNomeBairro() {
        return nomeBairro;
    }

    public void setNomeBairro(String nomeBairro) {
        this.nomeBairro = nomeBairro;
    }

    @Override
    public String toString() {
        return "Bairro{" +
                "nomeBairro='" + nomeBairro + '\'' +
                '}';
    }
}
