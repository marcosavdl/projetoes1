package com.example.es1_app.entity;

public class TipoLogradouro extends Entity{

    private String nomeTipo;

    public TipoLogradouro(){
        nomeTipo = "";
    }

    public TipoLogradouro(String nomeTipo){
        this.nomeTipo = nomeTipo;
    }

    public String getNomeTipo() {
        return nomeTipo;
    }

    public void setNomeTipo(String nomeTipo) {
        this.nomeTipo = nomeTipo;
    }

    @Override
    public String toString() {
        return "TipoLogradouro{" +
                "nomeTipo='" + nomeTipo + '\'' +
                '}';
    }
}
