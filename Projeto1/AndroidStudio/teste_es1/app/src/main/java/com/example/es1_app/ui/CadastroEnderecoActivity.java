package com.example.es1_app.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.es1_app.R;
import com.example.es1_app.entity.Bairro;
import com.example.es1_app.entity.Cidade;
import com.example.es1_app.entity.Endereco;
import com.example.es1_app.entity.Logradouro;
import com.example.es1_app.entity.UnidadeFederacao;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.hbb20.CountryCodePicker;

public class CadastroEnderecoActivity extends AppCompatActivity{

    private Endereco endereco;

    private TextInputEditText cep;
    private TextInputEditText lograd;    // Endereço
    private TextInputEditText numero;
    private TextInputEditText complemento;
    private TextInputEditText bairro;
    private TextInputEditText cidade;
    private TextInputEditText estado;
    private CountryCodePicker ccp_pais;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_endereco);

        setComponents();
        setViewAction();
    }

    public void setComponents(){

        endereco = new Endereco();
        cep = findViewById(R.id.textInputEditText_cep_cadastroEndereco);
        lograd = findViewById(R.id.textInputEditText_logradouro_cadastroEndereco);
        numero = findViewById(R.id.textInputEditText_numero_cadastroEndereco);
        complemento = findViewById(R.id.textInputEditText_complemento_cadastroEndereco);
        bairro = findViewById(R.id.textInputEditText_bairro_cadastroEndereco);
        cidade = findViewById(R.id.textInputEditText_cidade_cadastroEndereco);
        estado = findViewById(R.id.textInputEditText_estado_cadastroEndereco);
        ccp_pais = findViewById(R.id.CountryCodePicker_cadastroEndereco);
        button = findViewById(R.id.button_cadastroEndereco);
    }

    public void setViewAction(){

        cep.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(!hasFocus){
                    endereco.setCep(cep.getText().toString());
                }
            }
        });

        lograd.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(!hasFocus){
                    endereco.setLograd(new Logradouro(lograd.getText().toString()));
                }
            }
        });

        numero.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(!hasFocus){
                    endereco.setNumero(numero.getText().toString());
                }
            }
        });

        complemento.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(!hasFocus){
                    endereco.setComplemento(complemento.getText().toString());
                }
            }
        });

        bairro.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(!hasFocus){
                    endereco.setBairro(new Bairro(bairro.getText().toString()));
                }
            }
        });

        cidade.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(!hasFocus){
                    endereco.setCidade(new Cidade(cidade.getText().toString()));
                }
            }
        });

        // TODO Verificar Implementação Sigla-Nome
        estado.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(!hasFocus){
                    endereco.getCidade().setUf(new UnidadeFederacao());
                }
            }
        });

        // TODO Verificar inserção de país no banco de dados
        // ccp_pais

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //if(!validateFields(v)){
                if(false){

                }else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                    builder.setTitle("Cadastro de Endereço")
                            .setMessage(endereco.toString())
                            .setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            }).setNegativeButton("Voltar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    dialog.dismiss();
                                }
                    }).show();
                }
            }
        });
    }

    public boolean validateFields(View v){

        if(true){

            return true;
        }else{
            Snackbar.make(v,"Preencha os campos corretamente", Snackbar.LENGTH_LONG).show();
            return false;
        }
    }
}
