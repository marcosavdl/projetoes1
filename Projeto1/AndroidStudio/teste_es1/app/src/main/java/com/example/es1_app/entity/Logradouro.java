package com.example.es1_app.entity;

public class Logradouro extends Entity{

    private String nomeLograd;
    private TipoLogradouro tipoLograd;

    public Logradouro() {

        nomeLograd = "";
        tipoLograd = new TipoLogradouro();
    }

    public Logradouro(String nomeLograd) {
        this.nomeLograd = nomeLograd;
    }

    public Logradouro(String nomeLograd, TipoLogradouro tipoLograd){

        this.nomeLograd = nomeLograd;
        this.tipoLograd = tipoLograd;
    }

    public String getNomeLograd() {
        return nomeLograd;
    }

    public void setNomeLograd(String nomeLograd) {
        this.nomeLograd = nomeLograd;
    }

    public TipoLogradouro getTipoLograd() {
        return tipoLograd;
    }

    public void setTipoLograd(TipoLogradouro tipoLograd) {
        this.tipoLograd = tipoLograd;
    }

    @Override
    public String toString() {
        return "Logradouro{" +
                "nomeLograd='" + nomeLograd + '\'' +
                ", tipoLograd=" + tipoLograd +
                '}';
    }
}
